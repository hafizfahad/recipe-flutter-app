import 'package:global_configuration/global_configuration.dart';

//---------------API-base url-------------
final String baseUrl = GlobalConfiguration().get('api_base_url');
