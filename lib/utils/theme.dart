import 'package:flutter/material.dart';
import 'package:recipe_app/utils/colors.dart';

///-----------------------light-theme-settings----------------
lightTheme() => ThemeData(fontFamily: 'Poppins').copyWith(
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        },
      ),
      primaryColor: customThemeColor,
      appBarTheme: AppBarTheme(color: customThemeColor),
      scaffoldBackgroundColor: Color(0xffFFFFFF),
      buttonColor: customThemeColor,
      iconTheme: IconThemeData(
        color: customThemeColor,
      ),
      textTheme: TextTheme(
          button: TextStyle(
              fontFamily: 'PoppinsLight',
              fontSize: 12,
              color: customDarkBlackColor),
          subtitle1: TextStyle(
              fontFamily: 'PoppinsBold',
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: customThemeColor),
          subtitle2: TextStyle(
              fontFamily: 'PoppinsRegular',
              fontSize: 14,
              color: customDarkBlackColor),
          headline1: TextStyle(
              fontFamily: 'PoppinsBold',
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: customDarkBlackColor),
          headline2: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 9,
              fontWeight: FontWeight.bold,
              color: customDarkBlackColor),
          headline3: TextStyle(
              fontFamily: 'Poppins', fontSize: 10, color: customDarkBlackColor),
          headline4: TextStyle(
              fontFamily: 'PoppinsMedium',
              fontSize: 14,
              color: customDarkBlackColor),
          headline5: TextStyle(
              fontFamily: 'PoppinsSemiBold',
              fontSize: 14,
              color: customThemeColor),
          headline6: TextStyle()),
    );
