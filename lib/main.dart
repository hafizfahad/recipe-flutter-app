import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:recipe_app/controllers/app_controller.dart';
import 'package:recipe_app/route_generator.dart';
import 'package:recipe_app/utils/theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();

  //-----load-configurations-from-local-json
  await GlobalConfiguration().loadFromAsset("configurations");

  SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);

  runApp(InitClass());
}

class InitClass extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _InitClassState createState() => _InitClassState();
}

class _InitClassState extends State<InitClass> {
  @override
  Widget build(BuildContext context) {
    Get.put(AppController());

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      // initialRoute: PageRoutes.splash,
      getPages: routes(),
      theme: lightTheme(),
    );
  }
}
