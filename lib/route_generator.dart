import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:recipe_app/screens/splash_screen.dart';

routes() => [
      GetPage(name: "/", page: () => SplashScreen()),
    ];

class PageRoutes {
  static const String splash = '/';

  Map<String, WidgetBuilder> routes() {
    return {
      splash: (context) => SplashScreen(),
    };
  }
}
